package android.nguyenb.b1kp1kz;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class Snap extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        SnapFragment snap = new SnapFragment();
        getSupportFragmentManager().beginTransaction().add(R.id.container, snap).commit();
    }
}
