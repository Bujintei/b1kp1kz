package android.nguyenb.b1kp1kz;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class Profile extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ProfileFragment profile = new ProfileFragment();
        getSupportFragmentManager().beginTransaction().add(R.id.container, profile).commit();
    }
}
