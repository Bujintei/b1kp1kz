package android.nguyenb.b1kp1kz;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.backendless.Backendless;

public class MainActivity extends AppCompatActivity {

    public static final String APP_ID = "97C0398E-EF77-8117-FF69-B8B8B01B4500";
    public static final String SECRET_KEY = "6033833A-F22B-ED42-FF90-FF03049FFD00";
    public static final String VERSION = "v1";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log_reg);


        //MainMenuFragment mainMenu= new MainMenuFragment();
        //getSupportFragmentManager().beginTransaction().add(R.id.container, mainMenu).commit();

        Backendless.initApp(this, APP_ID, SECRET_KEY, VERSION);
        if(Backendless.UserService.loggedInUser() == "") {
            LogRegFragment logReg = new LogRegFragment();
            getSupportFragmentManager().beginTransaction().add(R.id.container, logReg).commit();
        } else {
            MainMenuFragment mainMenu = new MainMenuFragment();
            getSupportFragmentManager().beginTransaction().add(R.id.container, mainMenu).commit();
        }
    }
}
